<?php

namespace Loket\MetabaseSDK\Client;

use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;

trait MetabaseClient
{
    protected $headers = [];
    private function getContent($response)
    {
        $content = $response->getBody()->getContents();
        return @json_decode($content, 1) ? @json_decode($content, 1) : $content;
    }

    protected function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }

    protected function createRequest($method, $url, $parameters = [], $queyParams = [])
    {

        $httpClient    = new \GuzzleHttp\Client();
        $fullPath = getenv("METABASE_HOST") . $url;
        $defaultRequestParam = [
            "query"   => $queyParams,
            'headers'     => array_merge($this->headers, [
                "Accept" => "application/json",
            ])
        ];
        try {
            if ($method == "POST" || $method == "PUT") {
                $response = $httpClient->request($method, $fullPath, array_merge($defaultRequestParam, ["json" => $parameters]));
            } else {
                $response = $httpClient->request($method, $fullPath, $defaultRequestParam);
            }
            return $this->getContent($response);
        } catch (BadResponseException $exception) {
            $request  = $exception->getRequest();
            $response = $exception->getResponse();
            $content  = $this->getContent($response);
            return $content;
        } catch (RequestException $exception) {
            $request  = $exception->getRequest();
            $response = $exception->getResponse();
            $content  = $this->getContent($response);
            return $content;
        }
    }
}
