<?php

namespace Loket\MetabaseSDK\Client;

class Metabase
{
    use MetabaseClient;
    private static $instance = null;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
            $sessionAuth = self::$instance::getAuth()['id'];
            self::$instance->setHeaders(["X-Metabase-Session" => $sessionAuth]);
        }
        return self::$instance;
    }

    public static function getTopSales($cardId)
    {
        $endPoint =  sprintf("/card/%s/query/json", $cardId);
        $qParam = [
            "parameters" => sprintf("[{\"type\":\"category\",\"target\":[\"variable\",[\"template-tag\",\"minGMV1\"]],\"value\":%s},{\"type\":\"category\",\"target\":[\"variable\",[\"template-tag\",\"minGMV2\"]],\"value\":%s}]", getenv("GMV1"), getenv("GMV2")),
        ];
        return self::getInstance()->createRequest("POST", $endPoint, [], $qParam);
    }
    public static function getAuth(array $qParam = [])
    {
        $parameters = [
            'username' => getenv('METABASE_USERNAME'),
            'password' => getenv('METABASE_PASSWORD'),
        ];
        return self::getInstance()->createRequest("POST", "/session", $parameters, $qParam);
    }
}
